<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 18.04.2017
 * Time: 15:58
 */

namespace AppBundle\Twig\Extension;

use Doctrine\Common\Collections\ArrayCollection;
use Finance\ExpBundle\Entity\Doc;

class UserArraySortExtension extends \Twig_Extension
{
    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return array(
          new \Twig_SimpleFilter('usort', array($this, 'usortFilter'))
        );
    }

    /**
     * Сортирует коллекцию юзеров в алфавитном порядке
     * @param $item
     *
     * @return mixed
     */
    public function usortFilter($item)
    {
        if($item instanceof ArrayCollection){
            $item = $item->toArray();
        }

        usort($item, function ($item1, $item2) {

            $name1 = $item1->getUser()->getName();
            $name2 = $item2->getUser()->getName();

            $sortArr = [$name1, $name2];
            sort($sortArr);

            return ($name1 === $sortArr[0]) ? -1 : 1;
        });

        return $item;
    }
}