<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 14.03.2017
 * Time: 15:35
 */

namespace AppBundle\Twig\Extension;

use Symfony\Component\Routing\Router;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;


class BreadcrumbExtension extends \Twig_Extension
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var string
     */
    private $homeRoute;

    /**
     * @var string
     */
    private $homeLabel;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param Router $router
     * @param string $homeRoute
     * @param string $homeLabel
     */
    public function __construct(Breadcrumbs $breadcrumbs, Router $router, $homeRoute = 'homepage', $homeLabel = 'Home')
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->router = $router;
        $this->homeRoute = $homeRoute;
        $this->homeLabel = $homeLabel;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return array(
          new \Twig_SimpleFunction('breadcrumb', array($this, 'addBreadcrumb'))
        );
    }

    public function addBreadcrumb($label, $url = '', array $translationParameters = array())
    {
        if (!$this->breadcrumbs->count()) {
            $this->breadcrumbs->addItem($this->homeLabel, $this->router->generate($this->homeRoute));
        }
        $this->breadcrumbs->addItem($label, $url, $translationParameters);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'breadcrumb_extension';
    }
}