<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 06.04.2017
 * Time: 15:09
 */

namespace AppBundle\Entity;


use Symfony\Component\Security\Core\Role\RoleInterface;

class Role implements RoleInterface
{
    const ROLE_FINANCE_CURATOR = 'ROLE_FINANCE_CURATOR';
    const ROLE_FINANCE_INITIATOR = 'ROLE_FINANCE_INITIATOR';
    const ROLE_HEAD_DEPART = 'ROLE_HEAD_DEPART';
    const ROLE_EXP_SB = 'ROLE_EXP_SB';
    const ROLE_EXP_BUH = 'ROLE_EXP_BUH';
    const ROLE_EXP_FIN = 'ROLE_EXP_FIN';
    const ROLE_EXP_HEAD_LAWYER = 'ROLE_EXP_HEAD_LAWYER';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Role
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Role
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function getRole()
    {
        return $this->getName();
    }
}
