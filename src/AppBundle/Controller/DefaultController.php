<?php

namespace AppBundle\Controller;

use Acme\HwBundle\Entity\Tasks;
use AppBundle\Form\TaskType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/form2", name="new")
     * @Method("POST")
     */
    public function newFormAction(Request $request)
    {

        $entity = new \AppBundle\Entity\Tasks();
        $form = $this->createForm(TaskType::class, $entity);

        return $this->render('default/form1.html.twig',
          array(
            'entity' => $entity,
            'form' => $form->createView()
          )
        );



    }


    /**
     * @Route("/form1", name="form1")
     * @Method("POST")
     */
    public function formAction(Request $request)
    {
        //This is optional. Do not do this check if you want to call the same action using a regular request.
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }

        $post = new \AppBundle\Entity\Tasks();
        $form = $this->createForm(TaskType::class, $post);

        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() ) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return new JsonResponse(array('message' => 'Success! :)'), 200);
        }

        $response = new JsonResponse(
          array(
            'message' => 'Error',
            'form' => $this->renderView('default/form.chunk.html.twig',
              array(
                'entity' => $post,
                  'time' => time(),
                'form' => $form->createView(),
              ))), 400);

        return $response;

//        return $this->render('default/form1.html.twig', [
//          'form' => $form->createView()
//        ]);
        //return $this->render('default/form1.html.twig');
    }





    /**
     * Creates a new Demo entity.
     *
     * @Route("/", name="task_create")
     * @Method("POST")
     *
     */
    public function taskCreateAction(Request $request)
    {
        //This is optional. Do not do this check if you want to call the same action using a regular request.
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }

        /*$entity = new Demo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return new JsonResponse(array('message' => 'Success!'), 200);
        }*/

        $response = new JsonResponse(
          array(
            'message' => 'Error',
            'form' => $this->renderView('AcmeDemoBundle:Demo:form.html.twig',
              array(
                'entity' => $entity,
                'form' => $form->createView(),
              ))), 400);

        return $response;
    }



    /**
     * @Route("/contacts", name="contacts_list")
     * @Method("POST")
     */
    public function contactsListAction(Request $request)
    {
        $list = [
            '1' => [
                'username' => 'admin',
                'fio' => 'Ivanoff',
                'phone' => '22-33'
            ],
            '2' => [
              'username' => 'u2',
              'fio' => 'Kozlow',
              'phone' => '22-45'
            ],
            '3' => [
              'username' => 'u3',
              'fio' => 'Sidorov',
              'phone' => '65-33'
            ]
        ];

        //return $this->json(array('username' => !$request->isXmlHttpRequest()));
        //return new Response('<div>222 ' . $request->isXmlHttpRequest() . '</div>');

        return $this->render('default/chunk.ajax.html.twig', [
          'users' => $list,
        ]);
    }

    /**
     * @Route("/bookmarks", name="bookmarks")
     * @Method("POST")
     */
    public function bookmarksAction(Request $request)
    {
        //This is optional. Do not do this check if you want to call the same action using a regular request.
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }

        $repository = $this->getDoctrine()
          ->getRepository('AcmeHwBundle:Tasks')->findAll();

        return $this->render('default/chunk.ajax2.html.twig', [
          'repository' => $repository,
        ]);
    }

    /**
     * @Route("/contacts", name="contacts")
     */
    public function contactsAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('contacts.html.twig');
    }


}
