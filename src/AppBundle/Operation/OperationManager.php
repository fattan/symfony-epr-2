<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 11.04.2017
 * Time: 13:11
 */

namespace AppBundle\Operation;


class OperationManager
{
    private $operations = [];

    /**
     * @return array
     */
    public function getOperations(): array
    {
        return $this->operations;
    }

    /**
     * @param array ...$operations
     */
    public function __construct(... $operations)
    {
        $this->operations = $operations;
    }

}