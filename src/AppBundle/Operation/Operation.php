<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 11.04.2017
 * Time: 10:39
 */

namespace AppBundle\Operation;

class Operation
{

    const TYPE_REQUIRED = 'required';
    const TYPE_DEFAULT = 'default';
    const TYPE_READ_ONLY = 'read_only';

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $title;
    /**
     * @var bool
     */
    private $required = FALSE;
    /**
     * @var bool
     */
    private $form = FALSE;
    /**
     * @var bool
     */
    private $digitSign = FALSE;
    /**
     * @var
     */
    private $type;
    /**
     * @var int
     */
    private $sort = 100;

    private static $arrT = [];

    public function __construct(
        string $name,
        string $title,
        string $type = self::TYPE_DEFAULT,
        bool $required = false,
        bool $form = false,
        bool $digitSign = false,
        int $sort = 100
        )
    {
        $this->name = $name;
        $this->title = $title;
        $this->required = $required;
        $this->form = $form;
        $this->digitSign = $digitSign;
        $this->type = $type;
        $this->sort = $sort;
        self::$arrT[] = [
          'name' => $name,
          'title' => $title,
          'required' => $required,
          'form' => $form,
          'digitSign' => $digitSign,
          'type' => $type,
          'sort' => $sort,
          ];
    }

    public function getArrT2()
    {
        return self::$arrT;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @param bool $required
     */
    public function setRequired(bool $required)
    {
        $this->required = $required;
    }

    /**
     * @return bool
     */
    public function isForm(): bool
    {
        return $this->form;
    }

    /**
     * @param bool $form
     */
    public function setForm(bool $form)
    {
        $this->form = $form;
    }

    /**
     * @return bool
     */
    public function isDigitSign(): bool
    {
        return $this->digitSign;
    }

    /**
     * @param bool $digitSign
     */
    public function setDigitSign(bool $digitSign)
    {
        $this->digitSign = $digitSign;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort)
    {
        $this->sort = $sort;
    }



}