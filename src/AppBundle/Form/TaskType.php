<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 22.03.2017
 * Time: 10:51
 */
namespace AppBundle\Form;

use AppBundle\Entity\Tasks;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          //->add('title')
          ->add('name', TextareaType::class)
          ->add('userId', TextareaType::class)
          //->add('createdAt', DateTimeType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => Tasks::class,
        ));
    }
}