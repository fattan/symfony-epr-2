<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 20.04.2017
 * Time: 9:50
 */
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getUsersByRoleName($roleName)
    {
        return $this->createQueryBuilder('u')
          ->innerJoin('u.userRoles','r')
          ->where('r.name=?1')
          ->setParameter(1, $roleName)
          ->orderBy('u.name', 'ASC');
    }
}