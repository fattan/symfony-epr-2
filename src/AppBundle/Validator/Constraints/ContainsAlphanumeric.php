<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 22.03.2017
 * Time: 14:39
 */
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsAlphanumeric extends Constraint
{

    public $choices;
    public $provizion;

    public $message = 'Эта строка окошко "%string1%" не равно окошку "%string2%" ';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}