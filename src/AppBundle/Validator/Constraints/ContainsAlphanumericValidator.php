<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 22.03.2017
 * Time: 14:42
 */
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

class ContainsAlphanumericValidator extends ConstraintValidator
{
    
    public function validate($value, Constraint $constraint)
    {

        if ($value->getName() != $value->getUserId()) {
            $this->context->buildViolation($constraint->message)
              ->atPath('name')
              ->setParameter('%string1%', $value->getName())
              ->setParameter('%string2%', $value->getUserId())
              ->addViolation();
        }

        /*if ($constraint->provizion) {
            if (!is_callable($choices = array($this->context->getObject(), $constraint->provizion))
              && !is_callable($choices = array($this->context->getClassName(), $constraint->provizion))
              && !is_callable($choices = $constraint->provizion)
            ) {
                throw new ConstraintDefinitionException('The Choice constraint expects a valid callback');
            }
            $choices = call_user_func($choices);
        } else {
            $choices = $value;
        }


        if (!preg_match('/^[a-zA-Z0-9]+$/', $value, $matches)) {
            $this->context->buildViolation($constraint->message)
              //->setParameter('%string%', $value)
              ->setParameter('%string%', $choices)
              ->addViolation();
        }*/
    }
}