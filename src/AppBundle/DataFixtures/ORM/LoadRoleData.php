<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 21.04.2017
 * Time: 9:30
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Role;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $roleAdmin = new Role();
        $roleAdmin->setName('ROLE_ADMIN');
        $roleAdmin->setTitle('Админ всея ЁPR');

        $roelUser = new Role();
        $roelUser->setName('ROLE_USER');
        $roelUser->setTitle('ЮЗЕР всея ЁPR');

        $roelExp = new Role();
        $roelExp->setName('ROLE_FINANCE_INITIATOR');
        $roelExp->setTitle('Инициатор');

        $roelExp2 = new Role();
        $roelExp2->setName('ROLE_FINANCE_CURATOR');
        $roelExp2->setTitle('Куратор');

        $roelExp3 = new Role();
        $roelExp3->setName('ROLE_EXP_SB');
        $roelExp3->setTitle('СБ');

        $roelExp4 = new Role();
        $roelExp4->setName('ROLE_EXP_BUH');
        $roelExp4->setTitle('Бухгалтер');


        $roelExp5 = new Role();
        $roelExp5->setName('ROLE_EXP_FIN');
        $roelExp5->setTitle('Финансист');


        $roelExp6 = new Role();
        $roelExp6->setName('ROLE_EXP_HEAD_LAWYER');
        $roelExp6->setTitle('Нач. юр. службы');

        $roelExp7 = new Role();
        $roelExp7->setName('ROLE_FINANCE_EXTRA_AGREEMENT');
        $roelExp7->setTitle('Доп. согласующий');


        $manager->persist($roleAdmin);
        $manager->persist($roelUser);
        $manager->persist($roelExp);
        $manager->persist($roelExp2);
        $manager->persist($roelExp3);
        $manager->persist($roelExp4);
        $manager->persist($roelExp5);
        $manager->persist($roelExp6);
        $manager->persist($roelExp7);


        $this->makeRole(
          'ROLE_HEAD_DEPART',
          'Руководитель и/п',
          $manager
        );

        $manager->flush();

        $this->addReference('role-admin', $roleAdmin);
        $this->addReference('role-user', $roelUser);
        $this->addReference('role-exp-initiator', $roelExp);
        $this->addReference('role-exp-curator', $roelExp2);
        $this->addReference('role-exp-sb', $roelExp3);
        $this->addReference('role-exp-buh', $roelExp4);
        $this->addReference('role-exp-fin', $roelExp5);
        $this->addReference('role-exp-head-lawyer', $roelExp6);
        $this->addReference('role-exp-extra-agreement', $roelExp7);

    }

    public function makeRole($name, $title, $manager)
    {
        $role = new Role();
        $role->setName($name);
        $role->setTitle($title);
        $this->addReference($name, $role);
        $manager->persist($role);
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}
