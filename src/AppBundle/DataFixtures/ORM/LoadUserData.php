<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 20.04.2017
 * Time: 15:46
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();

        $userAdmin->setFirstName('Иван');
        $userAdmin->setMiddleName('Иванович');
        $userAdmin->setLastName('Иванов');

        $userAdmin->setUsername('admin');
        $userAdmin->setEmail('admin@ya.ru');
        $userAdmin->setPassword('1234');
        $userAdmin->addUserRole($this->getReference('role-admin'));
        $userAdmin->addUserRole($this->getReference('role-exp-initiator'));
        $userAdmin->addUserRole($this->getReference('role-exp-curator'));

        $user = new User();

        $user->setFirstName('Денис');
        $user->setMiddleName('Рифкатыч');
        $user->setLastName('Гиндуллин');


        $user->setUsername('user');
        $user->setEmail('user@ya.ru');
        $user->setPassword('1234');
        $user->addUserRole($this->getReference('ROLE_HEAD_DEPART'));
        $user->addUserRole($this->getReference('role-exp-initiator'));
        $user->addUserRole($this->getReference('role-exp-head-lawyer'));


        $userI = new User();

        $userI->setFirstName('Инициат');
        $userI->setMiddleName('Инициатыч');
        $userI->setLastName('Инициатов');

        $userI->setUsername('initiator');
        $userI->setEmail('initiator@ya.ru');
        $userI->setPassword('1234');
        $userI->addUserRole($this->getReference('role-exp-initiator'));

        $manager->persist($userI);
        $this->addReference('user-initiator', $userI);

        $userC = new User();

        $userC->setFirstName('Курат');
        $userC->setMiddleName('Курат');
        $userC->setLastName('Куратов');


        $userC->setUsername('curator');
        $userC->setEmail('curator@ya.ru');
        $userC->setPassword('1234');
        $userC->addUserRole($this->getReference('role-exp-curator'));

        $manager->persist($userC);
        $this->addReference('user-curator', $userC);


        $this->makeRoleUser(
          'role-exp-extra-agreement',
          'role-exp-extra-agreement1',
          'Согласян',
          $manager);

        $this->makeRoleUser(
          'role-exp-extra-agreement',
          'role-exp-extra-agreement2',
            'Согласян2',
            $manager);

        $this->makeRoleUser(
          'role-exp-extra-agreement',
          'role-exp-extra-agreement3',
          'Согласян3',
          $manager);

        $this->makeRoleUser(
          'ROLE_HEAD_DEPART',
          'ROLE_HEAD_DEPART',
          'Начайник1',
          $manager);


        for ($i = 0; $i < 4; $i++) {
            $userB = new User();

            $userB->setFirstName('Согласующий');
            $userB->setMiddleName('Согласующий' . $i);
            $userB->setLastName('FIN');

            $userB->setUsername('fin'  . $i);
            $userB->setEmail('fin'  . $i .  '@ya.ru');
            $userB->setPassword('1234');
            $userB->addUserRole($this->getReference('role-exp-fin'));

            $manager->persist($userB);
        }

        for ($i = 0; $i < 5; $i++) {
            $userB = new User();
            $userB->setFirstName('Согласующий');
            $userB->setMiddleName('Согласующий' . $i);
            $userB->setLastName('buh');
            $userB->setUsername('buh'  . $i);
            $userB->setEmail('b'  . $i .  '@ya.ru');
            $userB->setPassword('1234');
            $userB->addUserRole($this->getReference('role-user'));
            $userB->addUserRole($this->getReference('role-exp-buh'));

            $manager->persist($userB);
        }

        for ($i = 0; $i < 7; $i++) {
            $userB = new User();
            $userB->setFirstName('Согласующий');
            $userB->setMiddleName('Согласующий' . $i);
            $userB->setLastName('sb');
            $userB->setUsername('sb'  . $i);
            $userB->setEmail('sb'  . $i .  '@ya.ru');
            $userB->setPassword('1234');
            $userB->addUserRole($this->getReference('role-exp-sb'));

            $manager->persist($userB);
        }

        $manager->persist($userAdmin);
        $manager->persist($user);

        $manager->flush();

        $this->addReference('user-admin', $userAdmin);
    }

    public function makeRoleUser($role, $name, $userName, $manager)
    {
        $userEx2 = new User();

        $userEx2->setFirstName($userName);
        $userEx2->setMiddleName($userName);
        $userEx2->setLastName($userName);

        $userEx2->setUsername($name);
        $userEx2->setEmail($name . '@ya.ru');
        $userEx2->setPassword('1234');
        $userEx2->addUserRole($this->getReference($role));

        $manager->persist($userEx2);
        $this->addReference('user-' . $name, $userEx2);
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}