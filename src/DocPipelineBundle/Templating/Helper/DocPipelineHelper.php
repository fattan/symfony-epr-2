<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 24.04.2017
 * Time: 14:55
 */
namespace DocPipelineBundle\Templating\Helper;

use Symfony\Component\Templating\Helper\Helper;
use Symfony\Component\Templating\EngineInterface;
use DocPipelineBundle\Model\DocPipeline;

class DocPipelineHelper extends Helper
{
    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var DocPipeline
     */
    protected $docPipeline;

    /**
     * @var array The default options load from config file
     */
    protected $options = array();

    /**
     * @param \Symfony\Component\Templating\EngineInterface $templating
     * @param DocPipeline $operations
     * @param array $options The default options load from config file
     *
     */
    public function __construct(EngineInterface $templating, DocPipeline $operations, array $options = array())
    {
        $this->templating  = $templating;
        $this->docPipeline = $operations;
        $this->options = $options;
    }

    /**
     * Returns the HTML for the namespace breadcrumbs
     *
     * @param array $options The user-supplied options from the view
     * @return string A HTML string
     */
    public function docPipeline(array $options = array())
    {
        $options = $this->resolveOptions($options);

        $options['viewTemplate'] = 'DocPipelineBundle::doc_pipeline.html.twig';
        $docPipeline = $this->docPipeline;
        $options['route'] = $docPipeline->getRoute();
        $options['routeProgress'] = $docPipeline->getProgress();
        $options['doc'] = $docPipeline->doc;

        return $this->templating->render(
          $options["viewTemplate"],
          $options
        );
    }

    /**
     * Merges user-supplied options from the view
     * with base config values
     *
     * @param array $options The user-supplied options from the view
     * @return array
     */
    private function resolveOptions(array $options = array())
    {
        return array_merge($this->options, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return "doc_pipeline";
    }
}
