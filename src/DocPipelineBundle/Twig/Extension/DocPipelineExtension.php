<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 24.04.2017
 * Time: 14:45
 */
namespace DocPipelineBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Helper\Helper;

class DocPipelineExtension extends \Twig_Extension
{
    protected $container;
    protected $operations;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->operations = $container->get("doc_pipeline");
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
          //new \Twig_SimpleFunction("wo_operations", array($this, "getOperations")),
          new \Twig_SimpleFunction("render_doc_pipeline", array($this, "renderD"), array("is_safe" => array("html"))),
          new \Twig_SimpleFunction('doc_progress', array($this, 'docProgress'))
        );
    }

    public function docProgress($progress)
    {
        return $progress . '%';
    }

    /**
     * Renders the breadcrumbs in a list
     *
     * @param  array  $options
     * @return string
     */
    public function renderD(array $options = array())
    {
        return $this->container->get("doc_pipeline.helper")->docPipeline($options);
    }

}
