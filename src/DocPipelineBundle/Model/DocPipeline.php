<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 24.04.2017
 * Time: 14:14
 */
namespace DocPipelineBundle\Model;

class DocPipeline
{
    public $nodes = [];
    public $statusList = NULL;
    public $doc = NULL;
//    private $token;

    /**
     * @param $statusName
     *
     * @return string|null
     */
    public function getNextStatus($statusName)
    {
        foreach ($this->nodes as $node)
        {
            if($node->currStatus === $statusName){
                return $node->nextStatus;
            }
        }

        return null;
    }

    public function setDoc($doc)
    {
        $this->doc = $doc;
    }

    public function setStatusList($statusList)
    {
        $this->statusList = $statusList;
    }

    public function getStatusByName($name)
    {
        foreach ($this->statusList as $k => $v) {
            if($v->getName() === $name){
                return $v;
            }
        }
        return NULL;
    }

    /**
     * @param $status
     *
     * @return Node
     */
    public function createNode($status)
    {
        $node = new Node($this->doc);
        $node->currStatus = $status;
        return $node;
    }

    /**
     * @param array $config
     *
     * @return array
     */
    public function createNodes(array $config)
    {
        $node = [];
        foreach ($config as $item) {
            $node[] = $this->createNode($item['current'])
              ->next($item['next']);
        }

        return $node;
    }


    /**
     * $docPipeline->addNodes($node1, $node2);
     * OR
     * $docPipeline->addNodes([$node1, $node2]);
     *
     * @param array ...$nodes
     */
    public function addNodes(...$nodes)
    {
        $nodes = (count($nodes) === 1 && is_array($nodes))
          ? $nodes[0]
          : $nodes;

        foreach ($nodes as $k => $node){
            if($node instanceof Node){
                $node->statusO = $this->getStatusByName($node->currStatus);
                $this->nodes[] = $node;
            }
        }

    }

    public function getNextNode($node)
    {
        /**
         * Для текущей ноды
         * Из БД следедующий
         * статус берем
         */
        $nextStatus = (
          !is_null($this->doc->getNextStatus())
          && $node->currStatus === $this->doc->getStatus()->getName()
        )
          ? $this->doc->getNextStatus()->getName()
          : $node->nextStatus;

        foreach ($this->nodes as $k => $nextNode){
            if ($nextNode->currStatus === $nextStatus)
                return $nextNode;
        }
        return NULL;
    }

    public function getRoute()
    {
        foreach ($this->nodes as $k => $node){
            $node->nextNode = $this->getNextNode($node);
        }

        $nextNode = $this->nodes[0];
        $route = [];

        while (!is_null($nextNode)) {
            $route[] = $nextNode;
            $nextNode = $nextNode->nextNode;
        }

        return $route;
    }

    public function getProgress()
    {
        $route = $this->getRoute();
        $count = count($route);
        $i = 0;

        foreach ($route as $k => $node){
            $i++;
            if($this->doc->getStatus()->getName() === $node->currStatus)
            {
                break;
            }
        }

        return (int)(($i / $count) * 100);
    }


}
