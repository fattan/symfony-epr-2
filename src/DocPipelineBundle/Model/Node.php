<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 24.04.2017
 * Time: 14:35
 */

namespace DocPipelineBundle\Model;


use Finance\ExpBundle\Entity\RoleApprover;
use Finance\ExpBundle\Entity\Signature;
use Finance\ExpBundle\Entity\SignatureCollector;
use Finance\ExpBundle\Entity\UserApprover;

class Node
{
    public $currStatus = NULL;
    public $nextStatus = NULL;
    public $nextNode = NULL;
    public $ifTerm = NULL;
    public $ifStatus = NULL;
    public $elseIfTerm = [];
    public $elseTerm = [];
    public $repoStatus;
    public $statusO;
    private $doc;

    public $tmpS = null;

    public function __construct($doc)
    {
        $this->doc = $doc;
    }


    public function getDoc()
    {
        return $this->doc;
    }


    public function getApprovers()
    {
        $items = [
          'userType' => null,
          'roleType' => null,
        ];

        /** @var SignatureCollector $signatureCollector */
        $signatureCollector =  $this->getDoc()
          ->getSignatureCollectors()
          ->filter(function($item) {
            /** @var $item */
            return $item->getStatus()->getName() === $this->currStatus;
        })->first();

        if ($signatureCollector instanceof SignatureCollector) {
            $signatures = $signatureCollector->getSignatures();

            /** @var Signature $signature */
            foreach ($signatures as $signature){
                //dump($signature->getApprover()->getRole()->getName());
                //dump($signature->getApprover()->getUser());

                $approver = $signature->getApprover();

                if ($approver instanceof UserApprover) {

                    $items['userType'][] = [
                      'role' => $approver->getRole(),
                      'updatedAt' => $signature->getUpdatedAt(),
                      'result' => $signature->getResult(),
                      'user' => $signature->getUser(),
                      'defaultUser' => $approver->getUser()
                    ];


                } elseif ($approver instanceof RoleApprover) {

                    $items['roleType'][] = [
                      'role' => $approver->getRole(),
                      'updatedAt' => $signature->getUpdatedAt(),
                      'result' => $signature->getResult(),
                      'user' => (null !== $signature->getUser())
                        ? $signature->getUser()
                        : null
                    ];

                }

            }

        }

        return $items;
    }


    public function getSigns()
    {
        return $this->getDoc()->getSign()->filter(function($item) {
            /** @var $item */
            return $item->getStatus()->getName() === $this->currStatus;
        });
    }

    public function getStatusTitle()
    {
        return $this->statusO->getTitle();
    }

    public function getStatusId()
    {
        return $this->statusO->getId();
    }


    public function next($status)
    {
        $this->nextStatus = $status;
        return $this;
    }

    public function ifNext($condition, $status)
    {
        $this->ifTerm = $condition;
        $this->ifStatus = $status;
        return $this;
    }

    public function elseIfNext($condition, $status)
    {
        $this->elseIfTerm['status'] = $status;
        $this->elseIfTerm['condition'] = $condition;
        return $this;
    }

    public function elseNext($status)
    {
        $this->nextStatus = ($this->ifTerm) ? $this->ifStatus : $status;
        return $this;
    }
}
