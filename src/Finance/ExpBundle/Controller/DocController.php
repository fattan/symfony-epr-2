<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 22.03.2017
 * Time: 15:59
 */

namespace Finance\ExpBundle\Controller;

use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use Finance\ExpBundle\Builder\DocBuilder;
use Finance\ExpBundle\Entity\Approver;
use Finance\ExpBundle\Entity\Signature;
use Finance\ExpBundle\Entity\SignatureCollector;
use Finance\ExpBundle\Model\PipelineRouter;
use OperationsBundle\Form\OperationType;
use DateTime;
use Finance\ExpBundle\Entity\Doc;
use Finance\ExpBundle\Entity\Status;
use DocPipelineBundle\Model\DocPipeline;
use Finance\ExpBundle\Form\DocType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DocController extends Controller
{

    public function indexAction(Request $request, $page = 1)
    {
        $searchKey = 'q';
        $searchQuery = $request->query->get($searchKey, '');

        // Returns 5 posts out of 20
        $em = $this->getDoctrine()->getManager();
        $docs = $em->getRepository(Doc::class)
          ->getAllDocs($page, $searchQuery);

        // You can also call the count methods (check PHPDoc for `paginate()`)
        # Total fetched (ie: `5` posts)
        $totalPostsReturned = $docs->getIterator()->count();

        # Count of ALL posts (ie: `20` posts)
        $totalPosts = $docs->count();

        # ArrayIterator
        $iterator = $docs->getIterator();

        $limit = 20;
        $maxPages = ceil($totalPosts / $limit);
        $thisPage = $page;
        $pagePath = 'finance_exp_index';
        $viewPath = 'finance_exp_view';
        $categories = 'categories';

        return $this->render('list.html.twig', compact(
          'docs',
          'viewPath',
          'searchQuery',
          'searchKey',
          'pagePath',
          'iterator',
          'categories',
          'maxPages',
          'thisPage'));
    }

    public function viewAction($id)
    {
        /** @var \Finance\ExpBundle\Entity\Doc $doc */
        $doc = $this->getDoc($id);

        if (null === $doc) {
            throw new NotFoundHttpException('Запрашиваемый ресурс не найден');
        }

        //$this->denyAccessUnlessGranted('view', $doc);

        $this->getDocPipeline($doc);

        return $this->render('FinanceExpBundle:Doc:view.html.twig', [
            'doc' => $doc
        ]);
    }

    public function viewDocVerAction($id)
    {
        return $this->render('doc_ver_test.html.twig');
    }

    public function signCuratorAction(Request $request, $id)
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->ajaxOnlyAccessMessage();
        }

        $doc = $this->getDoc($id);
        $form = $this->createForm(TestType::class, $doc);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid()) {

            $this->setStatus($doc, Status::STATUS_ADD_ATTACHMENTS);

            return $this->json([
              'message' => 'success',
              'isValid' => $form->isValid()
            ], 200);
        }

        return $this->render(':operations:sign_only.html.twig', [
          'doc' => $doc,
          'action' => $request->get('_route'),
          'form' => $form->createView()
        ]);
    }

    public function onReworkAction($id)
    {
        $doc = $this->getDoc($id);
        $this->denyAccessUnlessGranted('on_rework', $doc);


        return new Response(' its onReworkAction ');
    }

    public function reworkAction(Request $request, $id)
    {
        $doc = $this->getDoc($id);
        $this->denyAccessUnlessGranted('rework', $doc);

        $form = $this->createForm(OperationType::class);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() ) {

            /** @var SignatureCollector $collector */
            $collector = $doc->getSignatureCollectors()->filter(function ($item) use($doc) {
                return $item->getStatus() === $doc->getStatus();
            })->first();
            // FIXME: domain logic shouldn`t be in a controller, move to model
            $doc->setStatus($this->getStatus(Status::STATUS_WAIT_HEAD_DEPART));
            $doc->setNextStatus($this->getStatus(Status::STATUS_WAIT_CURATOR));
            $doc->setStatusUpdatedAt(new \DateTime());


            /** @var Signature $signature */
            $signature = $collector->getSignatures()->first();
            $signature->setResult(1);
            $signature->setUser($this->getUser());
            $signature->setUpdatedAt(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($signature);
            $em->persist($doc);
            $em->flush();

            return $this->json([
              'message' => 'success',
              'isValid' => $form->isValid()
            ], 200);
        }

        return $this->render('OperationsBundle::form.html.twig', [
          'doc' => $doc,
          'action' => $request->get('_route'),
          'form' => $form->createView()
        ]);
    }

    public function viewLogAction($id)
    {
        $doc = $this->getDoc($id);
        $this->denyAccessUnlessGranted('view_log', $doc);

        return new Response(' its viewLogAction - ' . $doc->getName());
    }

    public function viewSignersListAction($docId, $statusId, $roleId)
    {
        $doc = $this->getDoc($docId);
        $signs = $doc->getSign()->filter(function($item) use($statusId, $roleId) {
            /** @var $item Sign */
            return (int) $item->getStatus()->getId() === (int) $statusId
              && (int) $item->getRole()->getId() === (int) $roleId;
        });


        return $this->render('::doc.signers.list.html.twig', [
          'signs' => $signs
        ]);
    }

    /**
     * TODO: Need a factory for creating instances (curator, initiator, etc.)
     * FIXME: Needs optimisation of dictionary load (statuses / roles)
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $docBuilder = new DocBuilder();
        $doc = $docBuilder->createDoc();

        $docBuilder->setInitiator($this->getRole(Role::ROLE_FINANCE_INITIATOR));
        $docBuilder->setCurator($this->getRole(Role::ROLE_FINANCE_CURATOR));
        $docBuilder->setHeadDepartment($this->getRole(Role::ROLE_HEAD_DEPART));


        $form = $this->createForm(DocType::class, $doc);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();

            $docBuilder->addSignatureCollector($this->getStatus(Status::STATUS_PROJECT));
            $docBuilder->addSignatureCollector($this->getStatus(Status::STATUS_WAIT_HEAD_DEPART));
            $docBuilder->addSignatureCollector($this->getStatus(Status::STATUS_WAIT_CURATOR));
            $docBuilder->addSignatureCollector($this->getStatus(Status::STATUS_WAIT_AGREEMENT));
            $docBuilder->addSignatureCollector($this->getStatus(Status::STATUS_WAIT_HEAD_LAWYER));
            $docBuilder->addSignatureCollector($this->getStatus(Status::STATUS_WAIT_LAWYER));
            $docBuilder->addSignatureCollector($this->getStatus(Status::STATUS_ADD_ATTACHMENTS));
            $docBuilder->addSignatureCollector($this->getStatus(Status::STATUS_CHECK_ATTACHMENTS));
            $docBuilder->addSignatureCollector($this->getStatus(Status::STATUS_CLOSE));

            /**
             * Approvers signatures
             */

            $docBuilder->addInitiatorSignature(Status::STATUS_PROJECT);
            $docBuilder->addInitiatorSignature(Status::STATUS_ADD_ATTACHMENTS);
            $docBuilder->addCuratorSignature(Status::STATUS_WAIT_CURATOR);
            $docBuilder->addHeadDepartmentSignature(Status::STATUS_WAIT_HEAD_DEPART);

            /**
             * согласующие
             */

            $docBuilder->addRoleApproverWithSignature($this->getRole(Role::ROLE_EXP_SB), Status::STATUS_WAIT_AGREEMENT);
            $docBuilder->addRoleApproverWithSignature($this->getRole(Role::ROLE_EXP_BUH), Status::STATUS_WAIT_AGREEMENT);
            $docBuilder->addRoleApproverWithSignature($this->getRole(Role::ROLE_EXP_FIN), Status::STATUS_WAIT_AGREEMENT);

            $docBuilder->addRoleApproverWithSignature($this->getRole(Role::ROLE_EXP_HEAD_LAWYER), Status::STATUS_WAIT_HEAD_LAWYER);

            /** @var SignatureCollector $collector */
            foreach ($doc->getSignatureCollectors() as $collector) {
                $em->persist($collector);
            }

            /** @var Approver $approver */
            foreach ($doc->getApprovers() as $approver){
                $em->persist($approver);
                foreach ($approver->getSignatures() as $signature){
                    $em->persist($signature);
                }
            }

            $currStatusName = Status::STATUS_PROJECT;
            $doc->setStatus($this->getStatus($currStatusName));
            $doc->setNextStatus(
              $this->getStatus(
                $this->getDocPipeline($doc, $currStatusName)
                  ->getNextStatus($currStatusName)
              )
            );

            $doc->setStatusUpdatedAt(new DateTime());
            $doc->setStatusDays(0);
            $doc->setCreatedAt(new \DateTime());
            $doc->setAuthor($em->getRepository(User::class)
              ->findOneBy(['username' => 'admin']));

            $dP = $this->getDocPipeline($doc, $currStatusName);
            $nodes = $dP->nodes;

            $em->persist($doc);
            $em->flush();

            return $this->redirectToRoute('finance_exp_view', ['id' => $doc->getId()]);
        }

        return $this->render('::doc.create.html.twig', [
          'form' => $form->createView()
        ]);
    }

    /**
     * @param Doc $doc
     *
     * @param null $start_status
     *
     * @return DocPipeline
     */
    private function getDocPipeline(Doc $doc, $start_status = null)
    {
        /** @var  DocPipeline $docPipeline */
        $docPipeline = $this->get('doc_pipeline');
        $docPipeline->setDoc($doc);

        if (null === $start_status) {
            $start_status = $doc->getStartStatus();
        }

        $router = new PipelineRouter($start_status);

        $node = $docPipeline->createNodes($router->getRoute());

        $em = $this->getDoctrine();
        $repoStatus = $em->getRepository(Status::class);
        $statusAll = $repoStatus->findAll();

        $docPipeline->setStatusList($statusAll);
        $docPipeline->addNodes($node);

        return $docPipeline;
    }

    /**
     * @param $doc
     * @param $statusName
     */
    private function setStatus(Doc $doc, $statusName)
    {
        $doc->setStatus($this->getStatus($statusName));
        $doc->setStatusUpdatedAt(new DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($doc);
        $em->flush();
    }


    private function getRole($name)
    {
        return $this->getDoctrine()
          ->getRepository(\AppBundle\Entity\Role::class)
          ->findOneBy(['name' => $name]);
    }

    /**
     * @param $id
     *
     * @return Doc|object
     */
    private function getDoc($id)
    {
        return $this->getRepositoryItem(Doc::class, $id);
    }

    /**
     * @param $name
     *
     * @return Status|object
     */
    private function getStatus($name)
    {
        return $this->getDoctrine()
          ->getRepository(Status::class)
          ->findOneBy(['name' => $name]);
    }

    /**
     * @param $class
     * @param $id
     *
     * @return object
     */
    private function getRepositoryItem($class, $id)
    {
        return $this->getDoctrine()
          ->getRepository($class)
          ->find($id);
    }

    private function ajaxOnlyAccessMessage(){
        return $this->json([
          'message' => 'You can access this only using Ajax!'
        ], 400);
    }

}
