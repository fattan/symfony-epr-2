<?php


namespace Finance\ExpBundle\Model;

use Finance\ExpBundle\Entity\Status;

class PipelineRouter
{
    private $route = [
        [
            'current' => Status::STATUS_PROJECT,
            'next' => Status::STATUS_WAIT_HEAD_DEPART,
        ],
        [
            'current' => Status::STATUS_WAIT_HEAD_DEPART,
            'next' => Status::STATUS_WAIT_CURATOR,
        ],
        [
            'current' => Status::STATUS_WAIT_CURATOR,
            'next' => Status::STATUS_WAIT_AGREEMENT,
        ],
        [
            'current' => Status::STATUS_WAIT_AGREEMENT,
            'next' => Status::STATUS_WAIT_HEAD_LAWYER
        ],
        [
            'current' => Status::STATUS_WAIT_HEAD_LAWYER,
            'next' => Status::STATUS_WAIT_LAWYER
        ],
        [
            'current' => Status::STATUS_WAIT_LAWYER,
            'next' => Status::STATUS_ADD_ATTACHMENTS,
        ],
        [
            'current' => Status::STATUS_ADD_ATTACHMENTS,
            'next' => Status::STATUS_CHECK_ATTACHMENTS,
        ],
        [
            'current' => Status::STATUS_CHECK_ATTACHMENTS,
            'next' => Status::STATUS_CLOSE,
        ],
        [
            'current' => Status::STATUS_CLOSE,
            'next' => null
        ],
    ];

    public function __construct($startStatus = null)
    {
        if ($startStatus) {
            $this->route[0]['current'] = $startStatus;
        }
    }

    public function getRoute()
    {
        return $this->route;
    }
}
