<?php

namespace Finance\ExpBundle\Entity;

use AppBundle\Entity\Role;


/**
 * Doc
 */
class Doc
{
    public function getStartStatus()
    {
        $versions = $this->getVersion();
        $versions->initialize();
        return (count($versions) > 0)
            ? Status::STATUS_REWORK
            : Status::STATUS_PROJECT;
    }

    public function getFinanceCurator()
    {
        return $this->getApproverByRoleName(Role::ROLE_FINANCE_CURATOR);
    }

    public function getFinanceInitiator()
    {
        return $this->getApproverByRoleName(Role::ROLE_FINANCE_INITIATOR);
    }

    public function getFinanceHeadDepart()
    {
        return $this->getApproverByRoleName(Role::ROLE_HEAD_DEPART);
    }

    /**
     * @param $roleName
     *
     * @return mixed
     */
    public function getApproverByRoleName($roleName)
    {
        return $this->getApprovers()->filter(function ($item) use ($roleName) {
            return $item->getRole()->getName() === $roleName;
        })->first();
    }


    /**
     * AUTO GEN:
     */


    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $statusUpdatedAt;

    /**
     * @var integer
     */
    private $statusDays;

    /**
     * @var string
     */
    private $contractNumber;

    /**
     * @var \DateTime
     */
    private $contractDate;

    /**
     * @var string
     */
    private $price;

    /**
     * @var string
     */
    private $currency = 'рос. руб.';

    /**
     * @var boolean
     */
    private $isOriginalReturned = false;

    /**
     * @var string
     */
    private $paymentProcedure;

    /**
     * @var string
     */
    private $deliveryTime;

    /**
     * @var string
     */
    private $otherConditions;

    /**
     * @var \DateTime
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \Finance\ExpBundle\Entity\CurrStatus
     */
    private $currStatus;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $approvers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $signatureCollectors;

    /**
     * @var \Finance\ExpBundle\Entity\Incoterms
     */
    private $incoterms;

    /**
     * @var \Finance\ExpBundle\Entity\Contragent
     */
    private $contragent;

    /**
     * @var \Finance\ExpBundle\Entity\Status
     */
    private $status;

    /**
     * @var \Finance\ExpBundle\Entity\Status
     */
    private $nextStatus;

    /**
     * @var \Finance\ExpBundle\Entity\doc
     */
    private $reg;

    /**
     * @var \AppBundle\Entity\User
     */
    private $author;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $version;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->approvers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->signatureCollectors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->version = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Doc
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set statusUpdatedAt
     *
     * @param \DateTime $statusUpdatedAt
     *
     * @return Doc
     */
    public function setStatusUpdatedAt($statusUpdatedAt)
    {
        $this->statusUpdatedAt = $statusUpdatedAt;

        return $this;
    }

    /**
     * Get statusUpdatedAt
     *
     * @return \DateTime
     */
    public function getStatusUpdatedAt()
    {
        return $this->statusUpdatedAt;
    }

    /**
     * Set statusDays
     *
     * @param integer $statusDays
     *
     * @return Doc
     */
    public function setStatusDays($statusDays)
    {
        $this->statusDays = $statusDays;

        return $this;
    }

    /**
     * Get statusDays
     *
     * @return integer
     */
    public function getStatusDays()
    {
        return $this->statusDays;
    }

    /**
     * Set contractNumber
     *
     * @param string $contractNumber
     *
     * @return Doc
     */
    public function setContractNumber($contractNumber)
    {
        $this->contractNumber = $contractNumber;

        return $this;
    }

    /**
     * Get contractNumber
     *
     * @return string
     */
    public function getContractNumber()
    {
        return $this->contractNumber;
    }

    /**
     * Set contractDate
     *
     * @param \DateTime $contractDate
     *
     * @return Doc
     */
    public function setContractDate($contractDate)
    {
        $this->contractDate = $contractDate;

        return $this;
    }

    /**
     * Get contractDate
     *
     * @return \DateTime
     */
    public function getContractDate()
    {
        return $this->contractDate;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Doc
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Doc
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set isOriginalReturned
     *
     * @param boolean $isOriginalReturned
     *
     * @return Doc
     */
    public function setIsOriginalReturned($isOriginalReturned)
    {
        $this->isOriginalReturned = $isOriginalReturned;

        return $this;
    }

    /**
     * Get isOriginalReturned
     *
     * @return boolean
     */
    public function getIsOriginalReturned()
    {
        return $this->isOriginalReturned;
    }

    /**
     * Set paymentProcedure
     *
     * @param string $paymentProcedure
     *
     * @return Doc
     */
    public function setPaymentProcedure($paymentProcedure)
    {
        $this->paymentProcedure = $paymentProcedure;

        return $this;
    }

    /**
     * Get paymentProcedure
     *
     * @return string
     */
    public function getPaymentProcedure()
    {
        return $this->paymentProcedure;
    }

    /**
     * Set deliveryTime
     *
     * @param string $deliveryTime
     *
     * @return Doc
     */
    public function setDeliveryTime($deliveryTime)
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    /**
     * Get deliveryTime
     *
     * @return string
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    /**
     * Set otherConditions
     *
     * @param string $otherConditions
     *
     * @return Doc
     */
    public function setOtherConditions($otherConditions)
    {
        $this->otherConditions = $otherConditions;

        return $this;
    }

    /**
     * Get otherConditions
     *
     * @return string
     */
    public function getOtherConditions()
    {
        return $this->otherConditions;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Doc
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set currStatus
     *
     * @param \Finance\ExpBundle\Entity\CurrStatus $currStatus
     *
     * @return Doc
     */
    public function setCurrStatus(\Finance\ExpBundle\Entity\CurrStatus $currStatus = null)
    {
        $this->currStatus = $currStatus;

        return $this;
    }

    /**
     * Get currStatus
     *
     * @return \Finance\ExpBundle\Entity\CurrStatus
     */
    public function getCurrStatus()
    {
        return $this->currStatus;
    }

    /**
     * Add approver
     *
     * @param \Finance\ExpBundle\Entity\Approver $approver
     *
     * @return Doc
     */
    public function addApprover(\Finance\ExpBundle\Entity\Approver $approver)
    {
        $this->approvers[] = $approver;

        return $this;
    }

    /**
     * Remove approver
     *
     * @param \Finance\ExpBundle\Entity\Approver $approver
     */
    public function removeApprover(\Finance\ExpBundle\Entity\Approver $approver)
    {
        $this->approvers->removeElement($approver);
    }

    /**
     * Get approvers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApprovers()
    {
        return $this->approvers;
    }

    /**
     * Add signatureCollector
     *
     * @param \Finance\ExpBundle\Entity\SignatureCollector $signatureCollector
     *
     * @return Doc
     */
    public function addSignatureCollector(\Finance\ExpBundle\Entity\SignatureCollector $signatureCollector)
    {
        $this->signatureCollectors[] = $signatureCollector;

        return $this;
    }

    /**
     * Remove signatureCollector
     *
     * @param \Finance\ExpBundle\Entity\SignatureCollector $signatureCollector
     */
    public function removeSignatureCollector(\Finance\ExpBundle\Entity\SignatureCollector $signatureCollector)
    {
        $this->signatureCollectors->removeElement($signatureCollector);
    }

    /**
     * Get signatureCollectors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSignatureCollectors()
    {
        return $this->signatureCollectors;
    }

    /**
     * Set incoterms
     *
     * @param \Finance\ExpBundle\Entity\Incoterms $incoterms
     *
     * @return Doc
     */
    public function setIncoterms(\Finance\ExpBundle\Entity\Incoterms $incoterms = null)
    {
        $this->incoterms = $incoterms;

        return $this;
    }

    /**
     * Get incoterms
     *
     * @return \Finance\ExpBundle\Entity\Incoterms
     */
    public function getIncoterms()
    {
        return $this->incoterms;
    }

    /**
     * Set contragent
     *
     * @param \Finance\ExpBundle\Entity\Contragent $contragent
     *
     * @return Doc
     */
    public function setContragent(\Finance\ExpBundle\Entity\Contragent $contragent = null)
    {
        $this->contragent = $contragent;

        return $this;
    }

    /**
     * Get contragent
     *
     * @return \Finance\ExpBundle\Entity\Contragent
     */
    public function getContragent()
    {
        return $this->contragent;
    }

    /**
     * Set status
     *
     * @param \Finance\ExpBundle\Entity\Status $status
     *
     * @return Doc
     */
    public function setStatus(\Finance\ExpBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Finance\ExpBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set nextStatus
     *
     * @param \Finance\ExpBundle\Entity\Status $nextStatus
     *
     * @return Doc
     */
    public function setNextStatus(\Finance\ExpBundle\Entity\Status $nextStatus = null)
    {
        $this->nextStatus = $nextStatus;

        return $this;
    }

    /**
     * Get nextStatus
     *
     * @return \Finance\ExpBundle\Entity\Status
     */
    public function getNextStatus()
    {
        return $this->nextStatus;
    }

    /**
     * Set reg
     *
     * @param \Finance\ExpBundle\Entity\doc $reg
     *
     * @return Doc
     */
    public function setReg(\Finance\ExpBundle\Entity\doc $reg = null)
    {
        $this->reg = $reg;

        return $this;
    }

    /**
     * Get reg
     *
     * @return \Finance\ExpBundle\Entity\doc
     */
    public function getReg()
    {
        return $this->reg;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return Doc
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add version
     *
     * @param \Finance\ExpBundle\Entity\Version $version
     *
     * @return Doc
     */
    public function addVersion(\Finance\ExpBundle\Entity\Version $version)
    {
        $this->version[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param \Finance\ExpBundle\Entity\Version $version
     */
    public function removeVersion(\Finance\ExpBundle\Entity\Version $version)
    {
        $this->version->removeElement($version);
    }

    /**
     * Get version
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersion()
    {
        return $this->version;
    }
}
