<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 04.05.2017
 * Time: 10:41
 */

namespace Finance\ExpBundle\Entity;


class SignatureCollector
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $result = 0;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $signatures;

    /**
     * @var \Finance\ExpBundle\Entity\Status
     */
    private $status;

    /**
     * @var \Finance\ExpBundle\Entity\Doc
     */
    private $doc;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->signatures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set result
     *
     * @param integer $result
     *
     * @return SignatureCollector
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return integer
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return SignatureCollector
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add signature
     *
     * @param \Finance\ExpBundle\Entity\Signature $signature
     *
     * @return SignatureCollector
     */
    public function addSignature(\Finance\ExpBundle\Entity\Signature $signature)
    {
        $this->signatures[] = $signature;

        return $this;
    }

    /**
     * Remove signature
     *
     * @param \Finance\ExpBundle\Entity\Signature $signature
     */
    public function removeSignature(\Finance\ExpBundle\Entity\Signature $signature)
    {
        $this->signatures->removeElement($signature);
    }

    /**
     * Get signatures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSignatures()
    {
        return $this->signatures;
    }

    /**
     * Set status
     *
     * @param \Finance\ExpBundle\Entity\Status $status
     *
     * @return SignatureCollector
     */
    public function setStatus(\Finance\ExpBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Finance\ExpBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set doc
     *
     * @param \Finance\ExpBundle\Entity\Doc $doc
     *
     * @return SignatureCollector
     */
    public function setDoc(\Finance\ExpBundle\Entity\Doc $doc = null)
    {
        $this->doc = $doc;

        return $this;
    }

    /**
     * Get doc
     *
     * @return \Finance\ExpBundle\Entity\Doc
     */
    public function getDoc()
    {
        return $this->doc;
    }
}
