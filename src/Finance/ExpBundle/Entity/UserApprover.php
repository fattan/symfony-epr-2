<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 03.05.2017
 * Time: 11:18
 */

namespace Finance\ExpBundle\Entity;


class UserApprover extends Approver
{

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UserApprover
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
