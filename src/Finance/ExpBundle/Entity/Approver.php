<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 03.05.2017
 * Time: 11:18
 */

namespace Finance\ExpBundle\Entity;


abstract class Approver
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $signatures;

    /**
     * @var \AppBundle\Entity\Role
     */
    private $role;

    /**
     * @var \Finance\ExpBundle\Entity\Doc
     */
    private $doc;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->signatures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add signature
     *
     * @param \Finance\ExpBundle\Entity\Signature $signature
     *
     * @return Approver
     */
    public function addSignature(\Finance\ExpBundle\Entity\Signature $signature)
    {
        $this->signatures[] = $signature;

        return $this;
    }

    /**
     * Remove signature
     *
     * @param \Finance\ExpBundle\Entity\Signature $signature
     */
    public function removeSignature(\Finance\ExpBundle\Entity\Signature $signature)
    {
        $this->signatures->removeElement($signature);
    }

    /**
     * Get signatures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSignatures()
    {
        return $this->signatures;
    }

    /**
     * Set role
     *
     * @param \AppBundle\Entity\Role $role
     *
     * @return Approver
     */
    public function setRole(\AppBundle\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \AppBundle\Entity\Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set doc
     *
     * @param \Finance\ExpBundle\Entity\Doc $doc
     *
     * @return Approver
     */
    public function setDoc(\Finance\ExpBundle\Entity\Doc $doc = null)
    {
        $this->doc = $doc;

        return $this;
    }

    /**
     * Get doc
     *
     * @return \Finance\ExpBundle\Entity\Doc
     */
    public function getDoc()
    {
        return $this->doc;
    }
}
