<?php

namespace Finance\ExpBundle\Entity;


/**
 * Status
 *
 */
class Status
{
    const STATUS_CANCEL = 'STATUS_CANCEL';
    const STATUS_PROJECT = 'STATUS_PROJECT';
    const STATUS_REWORK = 'STATUS_REWORK';
    const STATUS_WAIT_HEAD_DEPART = 'STATUS_WAIT_HEAD_DEPART';
    const STATUS_WAIT_CURATOR = 'STATUS_WAIT_CURATOR';
    const STATUS_WAIT_AGREEMENT = 'STATUS_WAIT_AGREEMENT';
    const STATUS_WAIT_HEAD_LAWYER = 'STATUS_WAIT_HEAD_LAWYER';
    const STATUS_WAIT_LAWYER = 'STATUS_WAIT_LAWYER';
    const STATUS_ADD_ATTACHMENTS = 'STATUS_ADD_ATTACHMENTS';
    const STATUS_CHECK_ATTACHMENTS = 'STATUS_CHECK_ATTACHMENTS';
    const STATUS_CLOSE = 'STATUS_CLOSE';

    /**
     * AUTO GEN
     */


    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $title;

    /**
     * @var integer
     */
    private $deadline = 0;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children1;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children2;

    /**
     * @var \Finance\ExpBundle\Entity\Status
     */
    private $prev;

    /**
     * @var \Finance\ExpBundle\Entity\Status
     */
    private $next;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children1 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children2 = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Status
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Status
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set deadline
     *
     * @param integer $deadline
     *
     * @return Status
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return integer
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Add children1
     *
     * @param \Finance\ExpBundle\Entity\Status $children1
     *
     * @return Status
     */
    public function addChildren1(\Finance\ExpBundle\Entity\Status $children1)
    {
        $this->children1[] = $children1;

        return $this;
    }

    /**
     * Remove children1
     *
     * @param \Finance\ExpBundle\Entity\Status $children1
     */
    public function removeChildren1(\Finance\ExpBundle\Entity\Status $children1)
    {
        $this->children1->removeElement($children1);
    }

    /**
     * Get children1
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren1()
    {
        return $this->children1;
    }

    /**
     * Add children2
     *
     * @param \Finance\ExpBundle\Entity\Status $children2
     *
     * @return Status
     */
    public function addChildren2(\Finance\ExpBundle\Entity\Status $children2)
    {
        $this->children2[] = $children2;

        return $this;
    }

    /**
     * Remove children2
     *
     * @param \Finance\ExpBundle\Entity\Status $children2
     */
    public function removeChildren2(\Finance\ExpBundle\Entity\Status $children2)
    {
        $this->children2->removeElement($children2);
    }

    /**
     * Get children2
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren2()
    {
        return $this->children2;
    }

    /**
     * Set prev
     *
     * @param \Finance\ExpBundle\Entity\Status $prev
     *
     * @return Status
     */
    public function setPrev(\Finance\ExpBundle\Entity\Status $prev = null)
    {
        $this->prev = $prev;

        return $this;
    }

    /**
     * Get prev
     *
     * @return \Finance\ExpBundle\Entity\Status
     */
    public function getPrev()
    {
        return $this->prev;
    }

    /**
     * Set next
     *
     * @param \Finance\ExpBundle\Entity\Status $next
     *
     * @return Status
     */
    public function setNext(\Finance\ExpBundle\Entity\Status $next = null)
    {
        $this->next = $next;

        return $this;
    }

    /**
     * Get next
     *
     * @return \Finance\ExpBundle\Entity\Status
     */
    public function getNext()
    {
        return $this->next;
    }
}
