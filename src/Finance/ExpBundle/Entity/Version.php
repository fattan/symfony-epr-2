<?php

namespace Finance\ExpBundle\Entity;


/**
 * Version
 *
 */
class Version
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $versionNumber;

    /**
     * @var string
     */
    private $data;

    /**
     * @var \DateTime
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $doc;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->doc = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set versionNumber
     *
     * @param integer $versionNumber
     *
     * @return Version
     */
    public function setVersionNumber($versionNumber)
    {
        $this->versionNumber = $versionNumber;

        return $this;
    }

    /**
     * Get versionNumber
     *
     * @return integer
     */
    public function getVersionNumber()
    {
        return $this->versionNumber;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Version
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Version
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add doc
     *
     * @param \Finance\ExpBundle\Entity\Doc $doc
     *
     * @return Version
     */
    public function addDoc(\Finance\ExpBundle\Entity\Doc $doc)
    {
        $this->doc[] = $doc;

        return $this;
    }

    /**
     * Remove doc
     *
     * @param \Finance\ExpBundle\Entity\Doc $doc
     */
    public function removeDoc(\Finance\ExpBundle\Entity\Doc $doc)
    {
        $this->doc->removeElement($doc);
    }

    /**
     * Get doc
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoc()
    {
        return $this->doc;
    }
}
