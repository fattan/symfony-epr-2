<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 03.05.2017
 * Time: 15:05
 */

namespace Finance\ExpBundle\Entity;


class Signature
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $result = 0;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \Finance\ExpBundle\Entity\SignatureCollector
     */
    private $signatureCollector;

    /**
     * @var \Finance\ExpBundle\Entity\Approver
     */
    private $approver;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set result
     *
     * @param integer $result
     *
     * @return Signature
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return integer
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Signature
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Signature
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set signatureCollector
     *
     * @param \Finance\ExpBundle\Entity\SignatureCollector $signatureCollector
     *
     * @return Signature
     */
    public function setSignatureCollector(\Finance\ExpBundle\Entity\SignatureCollector $signatureCollector = null)
    {
        $this->signatureCollector = $signatureCollector;

        return $this;
    }

    /**
     * Get signatureCollector
     *
     * @return \Finance\ExpBundle\Entity\SignatureCollector
     */
    public function getSignatureCollector()
    {
        return $this->signatureCollector;
    }

    /**
     * Set approver
     *
     * @param \Finance\ExpBundle\Entity\Approver $approver
     *
     * @return Signature
     */
    public function setApprover(\Finance\ExpBundle\Entity\Approver $approver = null)
    {
        $this->approver = $approver;

        return $this;
    }

    /**
     * Get approver
     *
     * @return \Finance\ExpBundle\Entity\Approver
     */
    public function getApprover()
    {
        return $this->approver;
    }
}
