<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 02.05.2017
 * Time: 13:08
 */

namespace Finance\ExpBundle\Entity;


class CurrStatus
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var integer
     */
    private $passedDays = '0';

    /**
     * @var \Finance\ExpBundle\Entity\Doc
     */
    private $doc;

    /**
     * @var \Finance\ExpBundle\Entity\Status
     */
    private $status;

    /**
     * @var \Finance\ExpBundle\Entity\Status
     */
    private $nextStatus;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return CurrStatus
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set passedDays
     *
     * @param integer $passedDays
     *
     * @return CurrStatus
     */
    public function setPassedDays($passedDays)
    {
        $this->passedDays = $passedDays;

        return $this;
    }

    /**
     * Get passedDays
     *
     * @return integer
     */
    public function getPassedDays()
    {
        return $this->passedDays;
    }

    /**
     * Set doc
     *
     * @param \Finance\ExpBundle\Entity\Doc $doc
     *
     * @return CurrStatus
     */
    public function setDoc(\Finance\ExpBundle\Entity\Doc $doc = null)
    {
        $this->doc = $doc;

        return $this;
    }

    /**
     * Get doc
     *
     * @return \Finance\ExpBundle\Entity\Doc
     */
    public function getDoc()
    {
        return $this->doc;
    }

    /**
     * Set status
     *
     * @param \Finance\ExpBundle\Entity\Status $status
     *
     * @return CurrStatus
     */
    public function setStatus(\Finance\ExpBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Finance\ExpBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set nextStatus
     *
     * @param \Finance\ExpBundle\Entity\Status $nextStatus
     *
     * @return CurrStatus
     */
    public function setNextStatus(\Finance\ExpBundle\Entity\Status $nextStatus = null)
    {
        $this->nextStatus = $nextStatus;

        return $this;
    }

    /**
     * Get nextStatus
     *
     * @return \Finance\ExpBundle\Entity\Status
     */
    public function getNextStatus()
    {
        return $this->nextStatus;
    }
}
