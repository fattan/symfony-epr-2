<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 20.04.2017
 * Time: 11:49
 */

namespace Finance\ExpBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finance\ExpBundle\Entity\Status;

class LoadStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $s1 = $this->createStatus('STATUS_CANCEL', 'Отменен');
        $manager->persist($s1);
        $statusProgect = $this->createStatus('STATUS_PROJECT', 'Проект (черновик)');
        $manager->persist($statusProgect);

        $s3 = $this->createStatus('STATUS_REWORK', 'Доработка');
        $manager->persist($s3);

        $s4 = $this->createStatus('STATUS_WAIT_HEAD_DEPART', 'Утверждение руководителем инициирующего подразделения');
        $manager->persist($s4);

        $statusWaitCurator = $this->createStatus('STATUS_WAIT_CURATOR', 'Утверждение куратором');
        $manager->persist($statusWaitCurator);

        $s6 = $this->createStatus('STATUS_WAIT_AGREEMENT', 'Согласование');
        $s7 = $this->createStatus('STATUS_WAIT_HEAD_LAWYER', 'Утверждение начальником юр. службы');
        $s8 = $this->createStatus('STATUS_WAIT_LAWYER', 'Подготовка документов юристом');
        $s9 = $this->createStatus('STATUS_ADD_ATTACHMENTS', 'Прикрепление документов инициатором');
        $s10 = $this->createStatus('STATUS_CHECK_ATTACHMENTS', 'Проверка документации юристом');
        $s11 = $this->createStatus('STATUS_CLOSE', 'Закрыт');

        $statusProgect->setNext($s4);
        $s4->setPrev($statusProgect);
        $s4->setNext($statusWaitCurator);
        $statusWaitCurator->setPrev($s4);
        $statusWaitCurator->setNext($s6);
        $s6->setPrev($statusWaitCurator);
        $s6->setNext($s7);
        $s7->setPrev($s6);
        $s7->setNext($s8);
        $s8->setPrev($s6);
        $s8->setNext($s9);
        $s9->setPrev($s8);
        $s9->setNext($s10);
        $s10->setPrev($s9);
        $s10->setNext($s11);
        $s11->setPrev($s10);

        $manager->persist($s6);
        $manager->persist($s7);
        $manager->persist($s8);
        $manager->persist($s9);
        $manager->persist($s10);
        $manager->persist($s11);

        $manager->flush();

        $this->addReference('status-project', $statusProgect);
        $this->addReference('status-wait-curator', $statusWaitCurator);
    }

    public function createStatus($name, $title)
    {
        $status = new Status();
        $status->setName($name);
        $status->setTitle($title);

        $this->addReference($name, $status);

        return $status;
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}