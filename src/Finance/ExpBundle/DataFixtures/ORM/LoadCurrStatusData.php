<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 02.05.2017
 * Time: 13:53
 */

namespace Finance\ExpBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finance\ExpBundle\Entity\CurrStatus;

class LoadCurrStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    private function createCurrStatus($number)
    {
        $stat = new CurrStatus();
        $stat->setStatus($this->getReference('status-project'));
        $stat->setDoc($this->getReference('exp-doc-' . $number));
        $stat->setUpdatedAt(new \DateTime());
        return $stat;
    }

    public function load(ObjectManager $manager)
    {
        $statuses = [];

        for ($i = 0; $i < 6; $i++)
        {
            $id = $i + 1;
            $status = $this->createCurrStatus($id);
            $statuses[] = $status;
            $manager->persist($status);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 6;
    }
}
