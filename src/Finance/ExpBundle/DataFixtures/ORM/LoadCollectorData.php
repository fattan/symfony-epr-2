<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 05.05.2017
 * Time: 8:20
 */

namespace Finance\ExpBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finance\ExpBundle\Entity\SignatureCollector;

class LoadCollectorData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createCollector('STATUS_PROJECT', $manager);
        $this->createCollector('STATUS_WAIT_HEAD_DEPART', $manager);
        $this->createCollector('STATUS_WAIT_CURATOR', $manager);
        $this->createCollector('STATUS_WAIT_AGREEMENT', $manager);
        $this->createCollector('STATUS_WAIT_HEAD_LAWYER', $manager);
        $this->createCollector('STATUS_WAIT_LAWYER', $manager);
        $this->createCollector('STATUS_ADD_ATTACHMENTS', $manager);
        $this->createCollector('STATUS_CHECK_ATTACHMENTS', $manager);

        $manager->flush();
    }

    public function createCollector($statusName, $manager)
    {
        $collector = new SignatureCollector();
        $collector->setStatus($this->getReference($statusName));
        $collector->setDoc($this->getReference('exp-doc-1'));
        $this->addReference('collector-' . $statusName, $collector);
        $manager->persist($collector);
    }


    public function getOrder()
    {
        return 6;
    }
}