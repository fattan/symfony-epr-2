<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 04.05.2017
 * Time: 14:10
 */

namespace Finance\ExpBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finance\ExpBundle\Entity\RoleApprover;
use Finance\ExpBundle\Entity\Signature;
use Finance\ExpBundle\Entity\SignatureCollector;
use Finance\ExpBundle\Entity\UserApprover;

class LoadApproverAndSignaturesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /**
         * INITIATOR
         */

        /** @var SignatureCollector $collector0 */
        $collector0 = $this->getReference('collector-STATUS_PROJECT');
        /** @var SignatureCollector $collector00 */
        $collector00 = $this->getReference('collector-STATUS_ADD_ATTACHMENTS');

        $app0 = new UserApprover();
        $app0->setDoc($this->getReference('exp-doc-1'));
        $app0->setRole($this->getReference('role-exp-initiator'));
        $app0->setUser($this->getReference('user-initiator'));

        $s1 = new Signature();
        $s1->setApprover($app0);
        $s1->setSignatureCollector($collector0);

        $s2 = new Signature();
        $s2->setApprover($app0);
        $s2->setSignatureCollector($collector00);

        $manager->persist($app0);
        $manager->persist($s1);
        $manager->persist($s2);


        /**
         * CURATOR
         */

        /** @var SignatureCollector $collector1 */
        $collector1 = $this->getReference('collector-STATUS_WAIT_CURATOR');

        $app1 = new UserApprover();
        $app1->setDoc($this->getReference('exp-doc-1'));
        $app1->setRole($this->getReference('role-exp-curator'));
        $app1->setUser($this->getReference('user-curator'));

        $sC = new Signature();
        $sC->setApprover($app1);
        $sC->setSignatureCollector($collector1);

        $manager->persist($app1);
        $manager->persist($sC);


        /**
         * HEAD_DEPART
         */

        /** @var SignatureCollector $collectorHD */
        $collectorHD = $this->getReference('collector-STATUS_WAIT_HEAD_DEPART');
        $uHD = $this->makeUserApprover(
          $this->getReference('exp-doc-1'),
          $this->getReference('ROLE_HEAD_DEPART'),
          $this->getReference('user-ROLE_HEAD_DEPART'),
          $manager);

        $this->makeSignature(
          $uHD,
          $collectorHD,
          $manager
        );

        /**
         * AGREEMENTS
         */

        /** @var SignatureCollector $collector2 */
        $collector2 = $this->getReference('collector-STATUS_WAIT_AGREEMENT');

        $uA1 = $this->makeUserApprover(
          $this->getReference('exp-doc-1'),
          $this->getReference('role-exp-extra-agreement'),
          $this->getReference('user-role-exp-extra-agreement1'),
          $manager);

        $this->makeSignature(
          $uA1,
          $collector2,
          $manager
        );

        $uA2 = $this->makeUserApprover(
          $this->getReference('exp-doc-1'),
          $this->getReference('role-exp-extra-agreement'),
          $this->getReference('user-role-exp-extra-agreement2'),
          $manager);

        $this->makeSignature(
          $uA2,
          $collector2,
          $manager
        );

        $uA3 = $this->makeUserApprover(
          $this->getReference('exp-doc-1'),
          $this->getReference('role-exp-extra-agreement'),
          $this->getReference('user-role-exp-extra-agreement3'),
          $manager);

        $this->makeSignature(
          $uA3,
          $collector2,
          $manager
        );


        $app2 = new RoleApprover();
        $app2->setDoc($this->getReference('exp-doc-1'));
        $app2->setRole($this->getReference('role-exp-sb'));

        $app3 = new RoleApprover();
        $app3->setDoc($this->getReference('exp-doc-1'));
        $app3->setRole($this->getReference('role-exp-fin'));

        $app4 = new RoleApprover();
        $app4->setDoc($this->getReference('exp-doc-1'));
        $app4->setRole($this->getReference('role-exp-buh'));


        $s3 = new Signature();
        $s3->setApprover($app2);
        $s3->setSignatureCollector($collector2);

        $s4 = new Signature();
        $s4->setApprover($app3);
        $s4->setSignatureCollector($collector2);

        $s5 = new Signature();
        $s5->setApprover($app4);
        $s5->setSignatureCollector($collector2);
        $s5->setUser($this->getReference('user-admin'));
        $s5->setResult(1);
        $s5->setUpdatedAt(new \DateTime());

        $manager->persist($app2);
        $manager->persist($app3);
        $manager->persist($app4);

        $manager->persist($s3);
        $manager->persist($s4);
        $manager->persist($s5);



        $manager->flush();
    }

    public function makeSignature($userApprover, $collector, $manager)
    {
        $signature = new Signature();
        $signature->setApprover($userApprover);
        $signature->setSignatureCollector($collector);

        $manager->persist($signature);

        return $signature;
    }

    public function makeUserApprover($doc, $role, $user, $manager)
    {
        $userApprover = new UserApprover();
        $userApprover->setDoc($doc);
        $userApprover->setRole($role);
        $userApprover->setUser($user);

        $manager->persist($userApprover);

        return $userApprover;
    }



    public function getOrder()
    {
        return 7;
    }
}