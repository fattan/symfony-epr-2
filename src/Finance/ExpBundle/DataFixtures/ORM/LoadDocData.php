<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 20.04.2017
 * Time: 11:03
 */
namespace Finance\ExpBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finance\ExpBundle\Entity\Contragent;
use Finance\ExpBundle\Entity\Doc;
use Finance\ExpBundle\Entity\Incoterms;
use Finance\ExpBundle\Entity\Signature;
use Finance\ExpBundle\Entity\Status;

class LoadDocData extends AbstractFixture implements OrderedFixtureInterface
{
    private  function createDoc($number)
    {
        $doc = new Doc();
        $doc->setStatus($this->getReference('status-project'));
        $doc->setStatusUpdatedAt(new DateTime());
        $doc->setName('Фикстура ' . $number);
        $doc->setStatusDays(0);
        $doc->setPrice(rand ( 100, 1000 ));
        $doc->setPaymentProcedure(rand ( 10, 10 ) . '% предоплата по безналичному расчету ');
        $doc->setDeliveryTime('Поставка в течение ' . rand ( 23, 100 ) . ' календарных дней со дня заключения настоящего договора при условии поступления предварительной оплаты ');
        $doc->setIncoterms($this->getReference('incoterms-CIF'));
        $doc->setContragent($this->getReference('contragent-1'));
        $doc->setAuthor($this->getReference('user-admin'));
        $doc->setCreatedAt(new \DateTime());
        return $doc;
    }

    public function load(ObjectManager $manager)
    {
        $docs = [];
        for ($i = 0; $i < 6; $i++)
        {
            $id = $i + 1;
            $doc = $this->createDoc($id);
            $docs[] = $doc;
            $manager->persist($doc);
        }

        $manager->flush();

        foreach ($docs as $key => $doc)
        {
            $id = $key + 1;
            $this->addReference("exp-doc-{$id}", $doc);
        }

    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 5;
    }
}
