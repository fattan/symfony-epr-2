<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 20.04.2017
 * Time: 11:34
 */

namespace Finance\ExpBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finance\ExpBundle\Entity\Contragent;

class LoadContragentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $contragent1 = new Contragent();
        $contragent1->setName('Закрытое акционерное общество «Свинокомплекс Ивановский»');
        $contragent1->setInn('3110011530');

        $manager->persist($contragent1);
        $manager->flush();

        $this->addReference('contragent-1', $contragent1);
    }


    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}