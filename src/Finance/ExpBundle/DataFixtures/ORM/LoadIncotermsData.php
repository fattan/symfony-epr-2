<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 20.04.2017
 * Time: 13:09
 */

namespace Finance\ExpBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finance\ExpBundle\Entity\Incoterms;

class LoadIncotermsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $manager->persist($this->createIncoterms('EXW', 'товар забирается покупателем с указанного в договоре склада продавца, оплата экспортных пошлин вменяется в обязанность покупателю'));
        $manager->persist($this->createIncoterms('FCA', 'товар доставляется основному перевозчику заказчика к указанному в договоре терминалу отправления, экспортные пошлины уплачивает продавец'));
        $manager->persist($this->createIncoterms('CPT', 'товар доставляется основному перевозчику заказчика, основную перевозку до указанного в договоре терминала прибытия оплачивает продавец, расходы по страховке несёт покупатель, импортную растаможку и доставку с терминала прибытия основного перевозчика осуществляет покупатель'));
        $manager->persist($this->createIncoterms('CIP', 'то же, что CPT, но основная перевозка страхуется продавцом'));
        $manager->persist($this->createIncoterms('DAT', 'поставка до указанного в договоре импортного таможенного терминала оплачена, то есть экспортные платежи и основную перевозку, включая страховку оплачивает продавец, таможенная очистка по импорту осуществляется покупателем'));
        $manager->persist($this->createIncoterms('DAP', 'поставка в место назначения, указанное в договоре, импортные пошлины и местные налоги оплачиваются покупателем'));
        $manager->persist($this->createIncoterms('DDP', 'товар доставляется заказчику в место назначения, указанное в договоре, очищенный от всех пошлин и рисков'));
        $manager->persist($this->createIncoterms('FOB', 'товар отгружается на судно покупателя, перевалку оплачивает продавец'));
        $manager->persist($this->createIncoterms('FAS', 'товар доставляется к судну покупателя, в договоре указывается порт погрузки, перевалку и погрузку оплачивает покупатель'));
        $manager->persist($this->createIncoterms('CFR', 'товар доставляется до указанного в договоре порта назначения покупателя, страховку основной перевозки, разгрузку и перевалку оплачивает покупатель'));

        $incotermsCIF = $this->createIncoterms('CIF', 'то же, что CFR, но основную перевозку страхует продавец');
        $manager->persist($incotermsCIF);

        $manager->flush();

        $this->addReference('incoterms-CIF', $incotermsCIF);
    }

    public function createIncoterms($name, $description)
    {
        $incoterms = new Incoterms();
        $incoterms->setName($name);
        $incoterms->setDescription($description);
        return $incoterms;
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}