<?php


namespace Finance\ExpBundle\Builder;


use AppBundle\Entity\Role;
use Finance\ExpBundle\Entity\Approver;
use Finance\ExpBundle\Entity\Doc;
use Finance\ExpBundle\Entity\RoleApprover;
use Finance\ExpBundle\Entity\Signature;
use Finance\ExpBundle\Entity\SignatureCollector;
use Finance\ExpBundle\Entity\Status;
use Finance\ExpBundle\Entity\UserApprover;

class DocBuilder
{
    /* @var Doc */
    private $doc;

    /* @var UserApprover */
    private $curator;

    /* @var UserApprover */
    private $initiator;

    /* @var UserApprover */
    private $headDepartment;

    public function createDoc() : Doc
    {
        $this->doc = new Doc();
        return $this->doc;
    }

    public function getDoc() : Doc
    {
        return $this-> doc;
    }

    public function setCurator(Role $role)
    {
        $this->curator = $this->createUserApprover($role);
    }

    public function setInitiator(Role $role)
    {
        $this->initiator = $this->createUserApprover($role);
    }

    public function setHeadDepartment(Role $role)
    {
        $this->headDepartment = $this->createUserApprover($role);
    }

    public function addCuratorSignature(string $status)
    {
        $this->createSignature($this->curator, $status);
    }

    public function addInitiatorSignature(string $status)
    {
        $this->createSignature($this->initiator, $status);
    }

    public function addHeadDepartmentSignature(string $status)
    {
        $this->createSignature($this->headDepartment, $status);
    }

    public function addSignatureCollector(Status $status)
    {
        $collector = new SignatureCollector();
        $collector->setStatus($status);
        $collector->setDoc($this->doc);
        $this->doc->addSignatureCollector($collector);
    }

    public function addRoleApproverWithSignature(Role $role, string $status)
    {
        $roleApprover = new RoleApprover();
        $roleApprover->setRole($role);
        $roleApprover->setDoc($this->doc);

        $this->doc->addApprover($roleApprover);

        $this->createSignature($roleApprover, $status);
    }

    private function createUserApprover(Role $role): UserApprover
    {
        $userApprover = new UserApprover();
        $userApprover->setRole($role);
        $userApprover->setDoc($this->doc);
        $this->doc->getApprovers()->add($userApprover);

        return $userApprover;
    }

    private function createSignature(Approver $approver, string $status)
    {
        $signature = new Signature();
        $signature->setApprover($approver);
        $signature->setSignatureCollector(
            $this->doc->getSignatureCollectors()->filter(function ($item) use($status) {
                return $item->getStatus()->getName() === $status;
            })->first()
        );

        $approver->addSignature($signature);
    }
}
