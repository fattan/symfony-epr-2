<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 19.04.2017
 * Time: 11:13
 */
namespace Finance\ExpBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class Test1 extends Constraint
{
//    public $choices;
//    public $provizion;

    public $message = 'Эта моя кастомная ошибка!';

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
        //return self::CLASS_CONSTRAINT;
    }
}