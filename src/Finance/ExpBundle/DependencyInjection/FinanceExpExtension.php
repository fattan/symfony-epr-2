<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 11.04.2017
 * Time: 13:20
 */

namespace Finance\ExpBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class FinanceExpExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
//        echo "<pre> ";
//        print_r($configs);
//        echo "</pre>";

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }
}