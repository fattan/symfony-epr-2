<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 06.04.2017
 * Time: 13:47
 */

namespace Finance\ExpBundle\Security;


use AppBundle\Entity\User;
use Finance\ExpBundle\Entity\Doc;
use Finance\ExpBundle\Entity\Status;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class DocVoter extends Voter
{
    // these strings are just invented: you can use anything
    const VIEW = 'view';
    const EDIT = 'edit';
    const AGREEMENT = 'agreement';
    const ON_REWORK = 'on_rework';
    const REWORK = 'rework';
    const SIGN_CURATOR = 'sign_curator';
    const VIEW_LOG = 'view_log';
    const ROLE_EXP_ADMIN = 'ROLE_EXP_ADMIN';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }


    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [
          self::VIEW,
          self::EDIT,
          self::AGREEMENT,
          self::ON_REWORK,
          self::REWORK,
          self::SIGN_CURATOR,
          self::VIEW_LOG,
          self::ROLE_EXP_ADMIN
        ])) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Doc) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // ROLE_SUPER_ADMIN can do anything! The power!
//        if ($this->decisionManager->decide($token, array('ROLE_EXP_ADMIN'))) {
//            return true;
//        }

        // you know $subject is a Doc object, thanks to supports
        /** @var Doc $doc */
        $doc = $subject;

        switch ($attribute) {
            //case self::ROLE_EXP_ADMIN: return false;
            case self::ON_REWORK: return true;
            case self::REWORK: return true;
            //case self::REWORK: return $this->canRework($doc, $user, $token);
            case self::VIEW_LOG: return true;
            case self::SIGN_CURATOR: return $this->canSignCurator($doc, $user, $token);
            case self::VIEW:
                return $this->canView($doc, $user);
            case self::EDIT:
                return $this->canEdit($doc, $user);
            case self::AGREEMENT:
                return $this->canAgreement($doc, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }


    private function canRework(Doc $doc, User $user, $token)
    {
        return
          $this->decisionManager->decide($token, array('ROLE_FINANCE_INITIATOR'))
          && $doc->getStatus()->getName() === Status::STATUS_PROJECT
          && $user === $doc->getFinanceInitiator()->getUser();
    }

    private function canView(Doc $doc, User $user)
    {
        // if they can edit, they can view
        if ($this->canEdit($doc, $user)) {
            return true;
        }

        // the Post object could have, for example, a method isPrivate()
        // that checks a boolean $private property
        //return !$doc->isPrivate();
        return true;
    }

    private function canEdit(Doc $doc, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return $user === $doc->getAuthor();
    }

    private function canSignCurator(Doc $doc, User $user, $token)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return ($doc->getStatus()->getId() === Status::STATUS_WAIT_CURATOR)
          && ($this->decisionManager->decide($token, array('ROLE_FINANCE_CURATOR')));
    }


    private function canAgreement(Doc $doc, User $user)
    {
        return true;
    }

}