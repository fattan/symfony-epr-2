<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 19.04.2017
 * Time: 11:17
 */

namespace Finance\ExpBundle\Form;

use Finance\ExpBundle\Entity\Contragent;
use Finance\ExpBundle\Entity\Doc;
use Finance\ExpBundle\Entity\Incoterms;
use NumberFormatter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('name', TextType::class, [
            'label' => 'Предмет договора',
            'attr' => ['autocomplete' => 'off'],
          ])
          ->add('price', NumberType::class, [
            'label' => 'Цена',
            'attr' => ['autocomplete' => 'off'],
            'scale' => 2,
            'grouping' => NumberFormatter::GROUPING_USED
          ])
          ->add('currency', TextType::class, [
            'label' => 'Валюта',
          ])
          ->add('incoterms', EntityType::class, [
            'class' => 'FinanceExpBundle:Incoterms',
            'label' => 'Условия поставки Инкотермс',
            'choice_label' => function ($category) {
              /** @var Incoterms $category */
                return $category->getName() . ' - ' . $category->getDescription();
            },
            'placeholder' => '--- Выберите ---'
          ])
          ->add('paymentProcedure', TextareaType::class, [
            'label' => 'Форма, сроки и порядок оплаты',
          ])
          ->add('deliveryTime', TextareaType::class, [
            'label' => 'Срок и порядок поставки'
          ])
          ->add('contragent', EntityType::class, [
            'class' => 'FinanceExpBundle:Contragent',
            'label' => 'Контрагент',
            'choice_label' => function ($category) {
                /** @var Contragent $category */
              $inn = (!empty($category->getInn())) ? $category->getInn() : 'не указан';
                return $category->getName() . ' (ИНН: ' . $inn . ')';
            },
            'placeholder' => '--- Выберите ---'
          ])
        ->add('approvers', CollectionType::class, [
          'entry_type' => ApproverType::class,
          'label' => 'Согласующие',
        ]);

        $builder->add('otherConditions', TextareaType::class, [
          'label' => 'Иные условия',
          'required' => false,
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => Doc::class,
        ));
    }
}
