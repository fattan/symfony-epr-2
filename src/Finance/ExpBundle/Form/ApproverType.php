<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 02.05.2017
 * Time: 16:56
 */

namespace Finance\ExpBundle\Form;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Finance\ExpBundle\Entity\Approver;
use Finance\ExpBundle\Entity\SingleSignature;
use Finance\ExpBundle\Entity\UserApprover;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApproverType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $listener = function (FormEvent $event) use($builder) {

            /** @var UserApprover $approver */
            if (!$approver = $event->getData()) {
                return;
            }

            $form = $event->getForm();
            $form->add('user', EntityType::class, [
              'class' => 'AppBundle:User',
              'label' => $approver->getRole()->getTitle(),
              'choice_label' => function ($user) {
                  /** @var User $user */
                  $absent = (!$user->getAbsent()) ? '' : ' (отсутствует)';
                  return $user->getFullName() . $absent;
              },
              'query_builder' => function (EntityRepository $er) use ($approver){
                  $qb = $er->createQueryBuilder('u')
                    ->innerJoin('u.userRoles','r')
                    ->where('r.name=?1')
                    ->setParameter(1, $approver->getRole()->getName())
                    ->orderBy('u.lastName', 'ASC')
                  ;
                  return $qb;
              },
              'placeholder' => '--- Выберите ---'
            ]);

        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, $listener);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => Approver::class,
        ));
    }
}