<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 23.03.2017
 * Time: 10:43
 */

namespace Finance\ExpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DocRepository extends EntityRepository
{
    /**
     * Our new getAllDocs() method
     *
     * 1. Create & pass query to paginate method
     * 2. Paginate will return a `\Doctrine\ORM\Tools\Pagination\Paginator` object
     * 3. Return that object to the controller
     *
     * @param integer $currentPage The current page (passed from controller)
     * @param string $searchQuery
     *
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAllDocs($currentPage = 1, $searchQuery = '')
    {
        $queryBuilder = $this->createQueryBuilder('d');

        if ($searchQuery) {
             $searchTerms = $this->extractSearchTerms($searchQuery);

            if (0 !== \count($searchTerms)) {
                foreach ($searchTerms as $key => $term) {
                    $queryBuilder
                        ->andWhere("d.name LIKE :term_{$key}")
                        ->setParameter("term_{$key}", "%{$term}%");
                }
            }
        }

        $queryBuilder
          ->orderBy('d.id', 'DESC')
          ->getQuery();

        // No need to manually get get the result ($query->getResult())

        $paginator = $this->paginate($queryBuilder, $currentPage);

        return $paginator;
    }

    /**
     * Paginator Helper
     *
     * Pass through a query object, current page & limit
     * the offset is calculated from the page and limit
     * returns an `Paginator` instance, which you can call the following on:
     *
     *     $paginator->getIterator()->count() # Total fetched (ie: `5` posts)
     *     $paginator->count() # Count of ALL posts (ie: `20` posts)
     *     $paginator->getIterator() # ArrayIterator
     *
     * @param \Doctrine\ORM\Query $dql   DQL Query Object
     * @param integer            $page  Current page (defaults to 1)
     * @param integer            $limit The total number per page (defaults to 5)
     *
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function paginate($dql, $page = 1, $limit = 20)
    {
        $paginator = new Paginator($dql);

        $paginator->getQuery()
          ->setFirstResult($limit * ($page - 1)) // Offset
          ->setMaxResults($limit); // Limit

        return $paginator;
    }

    /**
     * Transform the search string to an array of search terms
     * @param string $searchQuery
     * @return array
     */
    private function  extractSearchTerms(string $searchQuery): array
    {
        $searchTokens = trim(preg_replace('/[[:space:]]+/', ' ', $searchQuery));
        return array_unique(explode(' ', $searchTokens));
    }
}
