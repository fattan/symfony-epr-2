<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 02.05.2017
 * Time: 13:41
 */

namespace Finance\ExpBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Finance\ExpBundle\Entity\Doc;
use Finance\ExpBundle\Entity\MultiSignature;
use Finance\ExpBundle\Entity\RoleApproval;
use Finance\ExpBundle\Entity\RoleApprover;
use Finance\ExpBundle\Entity\Signature;
use Finance\ExpBundle\Entity\SignatureCollector;
use Finance\ExpBundle\Entity\SingleSignature;
use Finance\ExpBundle\Entity\Status;
use Finance\ExpBundle\Entity\UserApproval;
use Finance\ExpBundle\Entity\UserApprover;

class ApproveListener
{

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

//
//
//        if ($entity instanceof Doc) {
//
//            $doc = $entity;
//
//            if (!count($doc->getSignatureCollectors())) {
//                $collector = new SignatureCollector();
//                $collector->setStatus(
//                  $em->getRepository(Status::class)
//                    ->findOneBy(['name' => Status::STATUS_PROJECT])
//                );
//                $collector->setDoc($doc);
//                $em->persist($collector);
//
//            }
//
//
//        }

    }


    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {

            if ($entity instanceof Signature) {

                /** @var Signature $mySignature */
                $mySignature = $entity;
                $collector = $mySignature->getSignatureCollector();

                /** @var Signature $signature */
                foreach ($collector->getSignatures() as $signature){
                    if (!$signature->getResult()) return;
                }

                $collector->setResult(true);
                $collector->setUpdatedAt(new \DateTime());

                $this->persistEntity($em, $collector);
            }
        }
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param $entity
     */
    protected function persistEntity($em, $entity)
    {
        $em->getUnitOfWork()->recomputeSingleEntityChangeSet(
          $em->getClassMetaData(get_class($entity)),
          $entity
        );
    }
}