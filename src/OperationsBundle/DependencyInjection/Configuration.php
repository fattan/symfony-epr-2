<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 12.04.2017
 * Time: 13:38
 */

namespace OperationsBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
//        $rootNode = $treeBuilder->root('operations');
//
//        $rootNode
//          ->children()
//          ->scalarNode('param_one')->defaultValue('value_one')->end()
//          ->end()
//        ;

        return $treeBuilder;
    }
}