<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 12.04.2017
 * Time: 15:17
 */
namespace OperationsBundle\Templating\Helper;

use Symfony\Component\Templating\Helper\Helper;
use Symfony\Component\Templating\EngineInterface;
use OperationsBundle\Model\Operations;

class OperationsHelper extends Helper
{
    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var Operations
     */
    protected $operations;

    /**
     * @var array The default options load from config file
     */
    protected $options = array();

    /**
     * @param \Symfony\Component\Templating\EngineInterface $templating
     * @param Operations $operations
     * @param array $options The default options load from config file
     *
     */
    public function __construct(EngineInterface $templating, Operations $operations, array $options = array())
    {
        $this->templating  = $templating;
        $this->operations = $operations;
        $this->options = $options;
    }

    /**
     * Returns the HTML for the namespace breadcrumbs
     *
     * @param array $options The user-supplied options from the view
     * @return string A HTML string
     */
    public function operations(array $options = array())
    {
        $options = $this->resolveOptions($options);

        // Assign namespace breadcrumbs
        $options['operations']['default'] = $this->operations->getDefaultOperations();
        $options['operations']['required'] = $this->operations->getRequiredOperations();
        $options['operations']['readOnly'] = $this->operations->getReadOnlyOperations();

        if(!empty($options['routePrefix'])) {
            $this->operations->setRoutePrefix($options['routePrefix']);
        }


        $options['viewTemplate'] = 'OperationsBundle::operations.html.twig';

        return $this->templating->render(
          $options["viewTemplate"],
          $options
        );
    }

    /**
     * Merges user-supplied options from the view
     * with base config values
     *
     * @param array $options The user-supplied options from the view
     * @return array
     */
    private function resolveOptions(array $options = array())
    {
        return array_merge($this->options, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return "operations";
    }
}