<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 12.04.2017
 * Time: 8:28
 */

namespace OperationsBundle\Model;

class SingleOperation
{

    const TYPE_REQUIRED = 'required';
    const TYPE_DEFAULT = 'default';
    const TYPE_READ_ONLY = 'read_only';
    //const TYPE_LINK = 'link';

    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    private $routePrefix = '';
    /**
     * @var bool
     */
    private $form = FALSE;
    /**
     * @var bool
     */
    private $digitSign = FALSE;
    /**
     * @var
     */
    private $type;
    /**
     * @var int
     */
    public $sort = 100;

    public function __construct(string $name, string $title, string $type = self::TYPE_DEFAULT, int $sort = 100)
    {
        $this->support($type);

        $this->name = $name;
        $this->title = $title;
        $this->type = $type;
        $this->sort = $sort;

        switch ($type) {
            case self::TYPE_READ_ONLY:
            {
                $this->form = false;
                $this->digitSign = false;
                break;
            }
            default:
            {
                $this->form = true;
                $this->digitSign = true;
            }
        }
    }

    private function support($type)
    {
        if (!in_array($type, [
          self::TYPE_DEFAULT,
          self::TYPE_REQUIRED,
          self::TYPE_READ_ONLY
        ])) {
            throw new \LogicException('Тип операции не поддерживается!');
        }
    }

    public function getRoute()
    {
        return $this->getRoutePrefix() . $this->getName();
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return bool
     */
    public function isForm(): bool
    {
        return $this->form;
    }

    /**
     * @return bool
     */
    public function isDigitSign(): bool
    {
        return $this->digitSign;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getRoutePrefix(): string
    {
        return $this->routePrefix;
    }

    /**
     * @param string $routePrefix
     */
    public function setRoutePrefix(string $routePrefix)
    {
        $this->routePrefix = $routePrefix . '_';
    }


}