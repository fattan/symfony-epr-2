<?php
/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 12.04.2017
 * Time: 8:35
 */

namespace OperationsBundle\Model;


use AppBundle\Operation\Operation;

class Operations implements \Iterator, \ArrayAccess, \Countable
{

    private $operations = [];


    public function addItem($name, $title, $type = SingleOperation::TYPE_DEFAULT, $sort = 100)
    {
        $op = new SingleOperation($name, $title, $type, $sort);
        $this->operations[$name] = $op;
        return $this;
    }

    public function getOperations()
    {
        return $this->operations;
    }

    public function getRequiredOperations()
    {
        return $this->getOperationsByType(SingleOperation::TYPE_REQUIRED);
    }

    public function getDefaultOperations()
    {
        return $this->getOperationsByType(SingleOperation::TYPE_DEFAULT);
    }

    public function getReadOnlyOperations()
    {
        return $this->getOperationsByType(SingleOperation::TYPE_READ_ONLY);
    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getOperationsByType($type)
    {
        return array_filter(
          $this->operations,
          function ($operation) use($type){
              /** @var $operation SingleOperation */
            return $operation->getType() === $type;
        });
    }


    public function setRoutePrefix($prefix)
    {
        foreach ($this->operations as $k => $operation) {
            /** @var $operation SingleOperation */
            $operation->setRoutePrefix($prefix);
        }
    }

    public function sort($operations)
    {
        usort($operations, function($a, $b){
            return ($b->title - $a->sort);
        });

        return $operations;
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     *
     *
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return current($this->operations);
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        next($this->operations);
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return key($this->operations);
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return null !== key($this->operations);
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        reset($this->operations);
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     *
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     *
     *
     * @return bool true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->operations[$offset]);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     *
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     *
     *
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return isset($this->operations[$offset])
          ? $this->operations[$offset]
          : null;
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     *
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     *
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->operations[$offset] = $value;
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     *
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     *
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->operations[$offset]);
    }

    /**
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     *
     *
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count() {
        return count($this->operations);
    }

}