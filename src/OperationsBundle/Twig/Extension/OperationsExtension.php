<?php

/**
 * Created by PhpStorm.
 * User: marchenko_am
 * Date: 12.04.2017
 * Time: 14:57
 */
namespace OperationsBundle\Twig\Extension;

use OperationsBundle\Model\SingleOperation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Helper\Helper;


class OperationsExtension extends \Twig_Extension
{
    protected $container;
    protected $operations;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->operations = $container->get("operations");
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
          new \Twig_SimpleFunction("wo_operations", array($this, "getOperations")),
          new \Twig_SimpleFunction("wo_render_operations", array($this, "renderOperations"), array("is_safe" => array("html"))),
          new \Twig_SimpleFunction("wo_op2", array($this, "addOperation")),
        );
    }

    /**
     * Returns the breadcrumbs object
     *
     * @param string $namespace
     * @return \OperationsBundle\Model\Operations
     */
    public function getOperations()
    {
        return $this->operations;
    }

    public function addOperation($name, $title, $type = SingleOperation::TYPE_DEFAULT)
    {
        $this->operations->addItem($name, $title, $type);
    }

    /**
     * Renders the breadcrumbs in a list
     *
     * @param  array  $options
     * @return string
     */
    public function renderOperations(array $options = array())
    {
        return $this->container->get("operations.helper")->operations($options);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return "operations";
    }
}