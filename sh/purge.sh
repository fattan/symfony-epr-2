#!/bin/bash
echo 'Drop DB and create it again.'
php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
