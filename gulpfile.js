const { src, dest } = require('gulp');

const templateName = 'gentelella';
const nodeModulesFolder = 'node_modules';
const templatePath = `./${nodeModulesFolder}/${templateName}/`;

function copyDependencies() {
    return src([
        templatePath + 'vendors/**',
        templatePath + 'build/**',
        templatePath + 'production/*/**',
    ], { base: templatePath })
        .pipe(dest(`web/${templateName}/`));
}

exports.default = copyDependencies;
