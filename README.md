Attention: not for production usage!
========================

The project is still in development stage. You shouldn`t use it for production purposes.

What's inside?
--------------

- The Symfony 3 Standard Edition (PHP7, Composer, MySQL required)
- NPM package for the frontend build (Node >=12, NPM, Grunt required)
